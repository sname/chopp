class UrlsController < ApplicationController
  def create
    @url = Url.new(url_params)
    @url.user = current_user
    if @url.save
      respond_to do |format|
        format.js
      end
    end
  end

  def index
    @url = Url.new
  end

  def url_params
    params.require(:url).permit(:target)
  end
end
