class Url < ActiveRecord::Base
  before_create :shorten
  belongs_to :user

  validates :target, presence: true

  def shorten
    self.target = "http://" + target if target[0..3] != "http"
    self.chopped = get_short
  end

  def get_short
    len = %w(1 2 3 4)
    short = SecureRandom.hex(len.sample.to_i)
    get_short if Url.find_by(chopped: short)
    short
  end
end
