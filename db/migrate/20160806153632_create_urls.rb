class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :clicks, default: 0
      t.string :chopped
      t.string :target
      t.boolean :active, default: true
      t.timestamps null: false
    end
  end
end
